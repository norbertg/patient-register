module patientregister {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.mappe2.patientregister to javafx.fxml;
    opens edu.ntnu.idatt2001.mappe2.patientregister.controllers to javafx.fxml;
    opens edu.ntnu.idatt2001.mappe2.patientregister.models to javafx.base;

    exports edu.ntnu.idatt2001.mappe2.patientregister;
}
