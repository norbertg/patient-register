package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

public enum NodeType {
    STATUS_BAR,
    MENU_BAR,
    PATIENT_TABLE_VIEW,
    ACTION_BUTTONS,
}
