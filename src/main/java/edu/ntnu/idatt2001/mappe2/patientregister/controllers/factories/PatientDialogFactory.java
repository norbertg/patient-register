package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

import edu.ntnu.idatt2001.mappe2.patientregister.controllers.PatientDialogController;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PatientDialogFactory {
    private Stage createStage(String title) {
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.setTitle(title);
        stage.initModality(Modality.APPLICATION_MODAL);
        return stage;
    }

    /**
     * Creates a new PatientDialog based on the PatientDialogType.
     * @param patientDialogType The type of dialog to create.
     * @return New dialog of the specified type.
     * @throws IllegalArgumentException The PatientDialogType is not valid.
     */
    public PatientDialogController getPatientDialog(PatientDialogType patientDialogType) {
        String title;
        switch (patientDialogType) {
             case ADD:
                 title = "Patient Details - Add";
                 break;
             case EDIT:
                 title = "Patient Details - Edit";
                 break;
             default: throw new IllegalArgumentException(patientDialogType + " is not supported");
         }
         return new PatientDialogController(createStage(title));
    }
}
