package edu.ntnu.idatt2001.mappe2.patientregister.models;

import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.AlreadyExistsException;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.NotExistsException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class PatientRegister {
    private final HashMap<String, Patient> patients = new HashMap<>();

    /**
     * Adds a new patient to the PatientRegister.
     * @param patient Non-null patient
     * @throws AlreadyExistsException A patient with an equal social security number is already in the PatientRegister.
     */
    public void addPatient(Patient patient) throws AlreadyExistsException {
        Objects.requireNonNull(patient, "patient cannot be null");

        String socialSecurityNumber = patient.getSocialSecurityNumber();
        if (patients.containsKey(socialSecurityNumber)) {
            throw new AlreadyExistsException("patient exists already");
        }

        patients.put(socialSecurityNumber, patient);
    }

    public List<Patient> getPatients() {
        return new ArrayList<>(patients.values());
    }

    /**
     * Removes a patient from the PatientRegister.
     * The patient will be removed as patient, employee or both.
     * @param patient The non-null patient to be removed.
     * @throws NotExistsException The patient did not exist within the PatientRegister.
     */
    public void removePatient(Patient patient) throws NotExistsException {
        Objects.requireNonNull(patient, "patient cannot be null");

        String socialSecurityNumber = patient.getSocialSecurityNumber();
        if (!patients.containsKey(socialSecurityNumber)) {
            throw new NotExistsException("the patient does not exist within the PatientRegister");
        }

        patients.remove(socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "PatientRegister{" +
                ", patients=" + patients +
                '}';
    }
}
