package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

public enum PatientDialogType {
    ADD,
    EDIT,
}
