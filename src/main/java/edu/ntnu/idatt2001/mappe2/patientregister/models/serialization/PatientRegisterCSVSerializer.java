package edu.ntnu.idatt2001.mappe2.patientregister.models.serialization;

import edu.ntnu.idatt2001.mappe2.patientregister.models.Patient;
import edu.ntnu.idatt2001.mappe2.patientregister.models.PatientRegister;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.IllegalFormatException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

/**
 * Serializes a PatientRegister into a CSV stream.
 */
public class PatientRegisterCSVSerializer implements Closeable {
    private final OutputStream outputStream;

    /**
     * Output stream into which the PatientRegister will be serialized.
     * @param outputStream Non-null OutputStream instance.
     */
    public PatientRegisterCSVSerializer(OutputStream outputStream) {
        Objects.requireNonNull(outputStream, "outputStream cannot be null");

        this.outputStream = outputStream;
    }

    /**
     * Serializes the PatientRegister.
     * @throws IOException An IO exception has occurred.
     */
    public void serialize(PatientRegister patientRegister) throws IOException {
        List<Patient> patients = patientRegister.getPatients();
        try (OutputStreamWriter writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8)) {
            writer.write("firstName;lastName;socialSecurityNumber;diagnosis;generalPractitioner");
            for (Patient patient : patients) {
                writer.write(String.format(
                    "\n%s;%s;%s;%s;%s",
                    patient.getFirstName(),
                    patient.getLastName(),
                    patient.getSocialSecurityNumber(),
                    patient.getDiagnosis(),
                    patient.getGeneralPractitioner()
                ));
            }
        }
    }

    @Override
    public void close() throws IOException {
        outputStream.close();
    }
}
