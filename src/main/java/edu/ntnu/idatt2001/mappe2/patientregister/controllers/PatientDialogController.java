package edu.ntnu.idatt2001.mappe2.patientregister.controllers;

import edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories.AlertFactory;
import edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories.AlertType;
import edu.ntnu.idatt2001.mappe2.patientregister.models.Patient;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

public class PatientDialogController {
    @FunctionalInterface
    public interface OKHandler {
        boolean handle(Patient originalPatient, Patient newPatient);
    }

    @FXML private TextField firstNameTextField;
    @FXML private TextField lastNameTextField;
    @FXML private TextField socialSecurityNumberTextField;
    @FXML private TextField diagnosisTextField;
    @FXML private TextField generalPractitionerTextField;

    private final Stage stage;

    private Patient originalPatient;
    private OKHandler okHandler;

    /**
     * Creates a new PatientDialogController which uses the specified stage.
     * @param stage The stage to use. Cannot be null.
     */
    public PatientDialogController(Stage stage) {
        Objects.requireNonNull(stage, "stage cannot be null");
        this.stage = stage;

        FXMLLoader loader = new FXMLLoader(
            getClass().getResource("/edu/ntnu/idatt2001/mappe2/patientregister/views/PatientDialogView.fxml")
        );
        loader.setController(this);
        Parent parent;
        try {
            parent = loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        stage.setScene(new Scene(parent));
    }

    /**
     * Sets the the handler to use when the user presses OK.
     * @param okHandler The ok handler to use. Null indicates no handler.
     */
    public void setOKHandler(OKHandler okHandler) {
        this.okHandler = okHandler;
    }

    /**
     * Shows the dialog.
     */
    public void showAndWait() {
        stage.showAndWait();
    }

    /**
     * Set the patient from which the dialog data will be populated.
     * The patient will also be passed to the OKHandler as the originalPatient parameter.
     * @param patient Patient to use. Cannot be null.
     */
    public void setPatient(Patient patient) {
        Objects.requireNonNull(patient, "patient cannot be null");

        firstNameTextField.setText(patient.getFirstName());
        lastNameTextField.setText(patient.getLastName());
        socialSecurityNumberTextField.setText(patient.getSocialSecurityNumber());
        diagnosisTextField.setText(patient.getDiagnosis());
        generalPractitionerTextField.setText(patient.getGeneralPractitioner());
    }

    @FXML
    private void createPatient(ActionEvent event) {
        String socialSecurityNumber = socialSecurityNumberTextField.getText();
        if (!Patient.SOCIAL_SECURITY_NUMBER_PATTERN.matcher(socialSecurityNumber).matches()) {
            new AlertFactory()
                .getAlert(AlertType.INVALID_SOCIAL_SECURITY_NUMBER)
                .showAndWait();
            return;
        }
        if (okHandler != null) {
            Patient newPatient = new Patient(
                firstNameTextField.getText(),
                lastNameTextField.getText(),
                socialSecurityNumber
            );
            newPatient.setDiagnosis(diagnosisTextField.getText());
            newPatient.setGeneralPractitioner(generalPractitionerTextField.getText());
            if (okHandler.handle(originalPatient, newPatient)) {
                stage.close();
            }
        }
    }

    @FXML
    private void cancel(ActionEvent event) {
        stage.close();
    }
}
