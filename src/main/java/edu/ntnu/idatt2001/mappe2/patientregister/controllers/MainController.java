package edu.ntnu.idatt2001.mappe2.patientregister.controllers;

import edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories.*;
import edu.ntnu.idatt2001.mappe2.patientregister.models.Patient;
import edu.ntnu.idatt2001.mappe2.patientregister.models.PatientRegister;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.AlreadyExistsException;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.IllegalFormatException;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.NotExistsException;
import edu.ntnu.idatt2001.mappe2.patientregister.models.serialization.PatientRegisterCSVDeserializer;
import edu.ntnu.idatt2001.mappe2.patientregister.models.serialization.PatientRegisterCSVSerializer;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.*;
import java.util.Objects;
import java.util.Optional;

public class MainController {
    @FXML private TableColumn<Patient, Label> firstNameColumn;
    @FXML private TableColumn<Patient, Label> lastNameColumn;
    @FXML private TableColumn<Patient, Label> socialSecurityNumberColumn;
    @FXML private TableColumn<Patient, Label> diagnosisColumn;
    @FXML private TableColumn<Patient, Label> generalPractitionerColumn;
    @FXML private TableView<Patient> patientTableView;
    @FXML private Label statusLabel;

    private final Stage stage;
    private final SimpleStringProperty status = new SimpleStringProperty("OK");
    private ObservableList<Patient> patients;
    private PatientRegister patientRegister = new PatientRegister();

    /**
     * Creates a new MainController that uses the specified stage.
     * @param stage Stage to use. Cannot be null.
     */
    public MainController(Stage stage) {
        Objects.requireNonNull(stage, "stage cannot be null");
        this.stage = stage;

        FXMLLoader loader = new FXMLLoader(getClass().getResource(
            "/edu/ntnu/idatt2001/mappe2/patientregister/views/MainView.fxml"
        ));
        loader.setController(this);
        Parent parent;
        try {
            parent = loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        stage.setTitle("Patient Register");
        stage.setScene(new Scene(parent));
    }

    @FXML
    private void initialize() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));

        patients = patientTableView.getItems();
        statusLabel.textProperty().bind(new SimpleStringProperty("Status: ").concat(status));
    }

    private boolean showAlertIfFileIsNotCSV(File file) {
        String fileName = file.getName();
        String[] splitName = fileName.split("\\.");
        String fileExtension = "";
        if (splitName.length > 0) {
            fileExtension = splitName[splitName.length - 1];
        }
        boolean isNonCSV = !fileExtension.toLowerCase().equals("csv");
        if (isNonCSV) {
            new AlertFactory()
                .getAlert(AlertType.INVALID_FILE_EXTENSION)
                .showAndWait();
        }
        return isNonCSV;
    }

    private void showNoPatientSelectedAlert() {
        new AlertFactory()
            .getAlert(AlertType.NO_ITEM_SELECTED)
            .showAndWait();
    }

    private void removePatient(Patient patient) {
        try {
            patientRegister.removePatient(patient);
            patients.remove(patient);
        } catch (NotExistsException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean editPatient(Patient oldPatient, Patient patient) {
        removePatient(oldPatient);
        return addPatient(patient);
    }

    private boolean addPatient(Patient patient) {
        try {
            patientRegister.addPatient(patient);
            patients.add(patient);
            return true;
        } catch (AlreadyExistsException e) {
            new AlertFactory()
                .getAlert(AlertType.SOCIAL_SECURITY_NUMBER_ALREADY_EXISTS)
                .showAndWait();
            return false;
        }
    }

    @FXML
    private void onImportFromCSV(ActionEvent actionEvent) {
        File file = new FileChooserFactory()
            .getFileChooser(FileChooserType.OPEN_PATIENTS_CSV)
            .showOpenDialog(stage);
        if (file == null || showAlertIfFileIsNotCSV(file)) {
            return;
        }

        String nextStatus = "Import successful!";

        try (InputStream inputStream = new FileInputStream(file);
             PatientRegisterCSVDeserializer deserializer = new PatientRegisterCSVDeserializer(inputStream)
        ) {
            patientRegister = deserializer.deserialize();
        } catch (IllegalFormatException e) {
            nextStatus = "Error: " + e.getMessage();
        } catch (Exception e) {
            nextStatus = "Error: " + e.getMessage();
            e.printStackTrace();
        }

        patients.clear();
        patients.addAll(patientRegister.getPatients());
        patientTableView.refresh();

        status.set(nextStatus);
    }

    @FXML
    private void onExportToCSV(ActionEvent actionEvent) {
        // User will be asked whether they would like to overwrite
        // the file, and they will get the option to change the file.
        // This functionality is therefore not implemented manually.
        File file = new FileChooserFactory()
            .getFileChooser(FileChooserType.OPEN_PATIENTS_CSV)
            .showSaveDialog(stage);
        if (file == null || showAlertIfFileIsNotCSV(file)) {
            return;
        }

        String nextStatus = "Export successful!";

        try (OutputStream outputStream = new FileOutputStream(file);
             PatientRegisterCSVSerializer serializer = new PatientRegisterCSVSerializer(outputStream)
        ) {
            serializer.serialize(patientRegister);
        } catch (Exception e) {
            nextStatus = "Error: " + e.getMessage();
            e.printStackTrace();
        }

        status.set(nextStatus);
    }

    public void onExit(ActionEvent actionEvent) {
        Platform.exit();
    }

    @FXML
    private void onAddPatient(ActionEvent actionEvent) {
        PatientDialogController controller = new PatientDialogFactory()
            .getPatientDialog(PatientDialogType.ADD);
        controller.setOKHandler((oldPatient, newPatient) -> addPatient(newPatient));
        controller.showAndWait();
    }

    private Optional<Patient> getSelectedPatient() {
        return Optional.ofNullable(
            patientTableView.getSelectionModel().getSelectedItem()
        );
    }

    @FXML
    private void onEditPatient(ActionEvent actionEvent) {
        Optional<Patient> selectedPatient = getSelectedPatient();
        if (selectedPatient.isEmpty()) {
            showNoPatientSelectedAlert();
            return;
        }
        PatientDialogController controller = new PatientDialogFactory()
            .getPatientDialog(PatientDialogType.EDIT);
        controller.setPatient(selectedPatient.get());
        controller.setOKHandler(this::editPatient);
        controller.showAndWait();
    }


    @FXML
    private void onRemovePatient(ActionEvent actionEvent) {
        Optional<Patient> selectedPatient = getSelectedPatient();
        if (selectedPatient.isEmpty()) {
            showNoPatientSelectedAlert();
            return;
        }
        new AlertFactory()
            .getAlert(AlertType.DELETE_CONFIRMATION)
            .showAndWait()
            .filter(x -> x.getButtonData().isDefaultButton())
            .flatMap(x -> selectedPatient)
            .ifPresent(this::removePatient);
    }

    @FXML
    private void onAbout(ActionEvent actionEvent) {
        new AlertFactory()
            .getAlert(AlertType.ABOUT)
            .showAndWait();
    }
}
