package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

public enum FileChooserType {
    OPEN_PATIENTS_CSV,
    SAVE_PATIENTS_CSV;
}
