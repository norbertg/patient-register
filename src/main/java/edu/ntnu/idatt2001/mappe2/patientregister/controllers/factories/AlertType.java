package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

public enum AlertType {
    ABOUT,
    DELETE_CONFIRMATION,
    NO_ITEM_SELECTED,
    INVALID_FILE_EXTENSION,
    INVALID_SOCIAL_SECURITY_NUMBER,
    SOCIAL_SECURITY_NUMBER_ALREADY_EXISTS;
}
