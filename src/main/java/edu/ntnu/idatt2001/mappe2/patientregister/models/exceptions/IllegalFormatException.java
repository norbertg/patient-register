package edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions;

import java.io.IOException;

public class IllegalFormatException extends IOException {
    public IllegalFormatException() {
    }

    public IllegalFormatException(String message) {
        super(message);
    }

    public IllegalFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalFormatException(Throwable cause) {
        super(cause);
    }
}
