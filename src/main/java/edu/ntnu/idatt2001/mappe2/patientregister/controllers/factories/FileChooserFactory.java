package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

import javafx.stage.FileChooser;

public class FileChooserFactory {
    /**
     * Creates a new FileChooser based on the FileChooserType.
     * @param fileChooserType The type of FileChooser to create.
     * @return New FileChooser of the specified type.
     * @throws IllegalArgumentException The FileChooserType is not valid.
     */
    public FileChooser getFileChooser(FileChooserType fileChooserType) {
        FileChooser fileChooser = new FileChooser();
        switch (fileChooserType) {
            case OPEN_PATIENTS_CSV:
                fileChooser.setTitle("Open patients CSV file");
                fileChooser.getExtensionFilters().clear();
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                    "CSV file", "*.csv"
                ));
                return fileChooser;
            case SAVE_PATIENTS_CSV:
                fileChooser.setTitle("Save patients CSV file");
                fileChooser.getExtensionFilters().clear();
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                    "CSV file", "*.csv"
                ));
                return fileChooser;
            default:
                throw new IllegalArgumentException(fileChooserType + " is not supported");
        }
    }
}
