package edu.ntnu.idatt2001.mappe2.patientregister.models;

import java.util.Objects;
import java.util.regex.Pattern;

public class Patient {
    public static final Pattern SOCIAL_SECURITY_NUMBER_PATTERN = Pattern.compile("\\d{11}");

    private final String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Creates a new patient.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     * @param diagnosis Cannot be null.
     * @param generalPractitioner Cannot be null.
     */
    public Patient(
        String firstName,
        String lastName,
        String socialSecurityNumber,
        String diagnosis,
        String generalPractitioner
    ) {
        Objects.requireNonNull(firstName, "firstName cannot be null");
        Objects.requireNonNull(lastName, "lastName cannot be null");
        Objects.requireNonNull(socialSecurityNumber, "socialSecurityNumber cannot be null");
        Objects.requireNonNull(diagnosis, "diagnosis cannot be null");
        Objects.requireNonNull(generalPractitioner, "generalPractitioner cannot be null");

        if (!SOCIAL_SECURITY_NUMBER_PATTERN.matcher(socialSecurityNumber).matches()) {
            throw new IllegalArgumentException("socialSecurityNumber must be an 11 digit number");
        }

        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Creates a new patient with diagnosis and generalPractitioner set to empty.
     * @param firstName Cannot be blank or null.
     * @param lastName Cannot be blank or null.
     * @param socialSecurityNumber Cannot be blank or null.
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        this(firstName, lastName, socialSecurityNumber, "", "");
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the person's first name.
     * @param firstName cannot be blank or null.
     */
    public void setFirstName(String firstName) {
        Objects.requireNonNull(firstName, "firstName");
        if (firstName.isBlank()) {
            throw new IllegalArgumentException("firstName cannot be null or blank");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the person's last name.
     * @param lastName cannot be blank or null.
     */
    public void setLastName(String lastName) {
        Objects.requireNonNull(lastName, "lastName cannot be null");
        if (lastName.isBlank()) {
            throw new IllegalArgumentException("firstName cannot be null or blank");
        }
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        Objects.requireNonNull(diagnosis, "diagnosis cannot be null");
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Sets the patient's general practitioner.
     * @param generalPractitioner cannot be null.
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        Objects.requireNonNull(generalPractitioner, "generalPracitioner cannot be null");
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public String toString() {
        return "Patient{" +
            "socialSecurityNumber='" + socialSecurityNumber + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", diagnosis='" + diagnosis + '\'' +
            ", generalPractitioner='" + generalPractitioner + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient person = (Patient) o;
        return socialSecurityNumber.equals(person.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
