package edu.ntnu.idatt2001.mappe2.patientregister;

import edu.ntnu.idatt2001.mappe2.patientregister.controllers.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;

public class Main extends Application {

    private void loadFont() {
        try (InputStream fontStream = getClass().getResourceAsStream("/edu/ntnu/idatt2001/mappe2/patientregister/fonts/font-awesome-5-free-solid-900.otf")) {
            Font fontAwesome = Font.loadFont(fontStream, 0);
            if (fontAwesome == null) {
                System.err.println("unable to load Font Awesome");
            }
        } catch (IOException e) {
            System.err.println("unable to load Font Awesome: " + e.getMessage());
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new MainController(primaryStage);
        loadFont();
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
