package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;

public class AlertFactory {
    private Alert createAlert(Alert.AlertType type, String title, String header, String content) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        return alert;
    }

    /**
     * Creates a new alert based on the AlertType.
     * @param alertType The type of alert to create.
     * @return New alert of the specified type.
     * @throws IllegalArgumentException The AlertType is not valid.
     */
    public Alert getAlert(AlertType alertType) {
        switch (alertType) {
            case ABOUT: return createAlert(
                Alert.AlertType.INFORMATION,
                "About",
                "Patients Register\n" +
                    getClass().getModule().getDescriptor().version().orElseThrow(),
                "Application by:\n" +
                    "Norbert Gørke (norbertg@stud.ntnu.no)"
            );
            case DELETE_CONFIRMATION: return createAlert(
                Alert.AlertType.CONFIRMATION,
                "Delete confirmation",
                "Delete confirmation",
                "Are you sure you want to delete this item?"
            );
            case NO_ITEM_SELECTED: return createAlert(
                Alert.AlertType.WARNING,
                "No item selected",
                "No item selected",
                "Please select an item and try again."
            );
            case INVALID_SOCIAL_SECURITY_NUMBER: return createAlert(
                Alert.AlertType.ERROR,
                "Invalid social security number",
                "Invalid social security number",
                "The social security number must be an 11-digit number."
            );
            case INVALID_FILE_EXTENSION: return createAlert(
                Alert.AlertType.ERROR,
                "Invalid file type",
                "Invalid file type",
                "The chosen file type is not valid.\n" +
                    "Please try again with a *.csv file."
            );
            case SOCIAL_SECURITY_NUMBER_ALREADY_EXISTS: return createAlert(
                Alert.AlertType.ERROR,
                "Patient already exists",
                "Patient already exists",
                "A patient with the specified social security number\n" +
                    "already exists."
            );
            default: throw new IllegalArgumentException(alertType + " is not supported");
        }
    }
}
