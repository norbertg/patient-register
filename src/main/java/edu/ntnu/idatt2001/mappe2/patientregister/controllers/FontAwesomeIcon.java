package edu.ntnu.idatt2001.mappe2.patientregister.controllers;

/**
 * Unicode values for FontAwesome icons.
 */
public enum FontAwesomeIcon {
    USER_PLUS("\uf234"),
    USER_MINUS("\uf503"),
    USER_EDIT("\uf4ff");

    private final String code;

    @Override
    public String toString() {
        return code;
    }

    FontAwesomeIcon(String code) {
        this.code = code;
    }
}
