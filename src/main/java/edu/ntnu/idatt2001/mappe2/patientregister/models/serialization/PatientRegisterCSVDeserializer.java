package edu.ntnu.idatt2001.mappe2.patientregister.models.serialization;

import edu.ntnu.idatt2001.mappe2.patientregister.models.Patient;
import edu.ntnu.idatt2001.mappe2.patientregister.models.PatientRegister;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.AlreadyExistsException;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.IllegalFormatException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Deserializes a PatientRegister from a CSV stream.
 */
public class PatientRegisterCSVDeserializer implements Closeable {
    private static final Pattern DELIMITER = Pattern.compile(";");
    private final InputStream inputStream;

    /**
     * Input stream from which the PatientRegister will be deserialized.
     * @param inputStream Non-null InputStream instance.
     */
    public PatientRegisterCSVDeserializer(InputStream inputStream) {
        Objects.requireNonNull(inputStream, "inputStream cannot be null");

        this.inputStream = inputStream;
    }

    private Patient createPatientFromValues(Map<String, String> values) throws IllegalFormatException {
        String socialSecurityNumber = values.getOrDefault("socialSecurityNumber", "");

        if (!Patient.SOCIAL_SECURITY_NUMBER_PATTERN.matcher(socialSecurityNumber).matches()) {
            throw new IllegalFormatException("Social security number must be an 11-digit number");
        }

        Patient patient = new Patient(
            values.getOrDefault("firstName", ""),
            values.getOrDefault("lastName", ""),
            socialSecurityNumber,
            values.getOrDefault("diagnosis", ""),
            values.getOrDefault("generalPractitioner", "")
        );

        return patient;
    }

    private Optional<Patient> patientFromRow(String[] columnNames, String row) throws IllegalFormatException {
        if (row.trim().isEmpty()) {
            return Optional.empty();
        }

        HashMap<String, String> values = new HashMap<>(columnNames.length);
        String[] rowValues = DELIMITER.split(row);
        if (rowValues.length != columnNames.length) {
            throw new IllegalFormatException("A row with invalid number of values was present.");
        }
        for (int i = 0; i < columnNames.length; i++) {
            values.put(columnNames[i], rowValues[i]);
        }

        return Optional.of(createPatientFromValues(values));
    }

    /**
     * Deserializes the PatientRegister.
     * @return PatientRegister which was deserialized.
     * @throws IOException An IO exception has occurred.
     * @throws IllegalFormatException The CSV file had an illegal format.
     */
    public PatientRegister deserialize() throws IOException {
        PatientRegister register = new PatientRegister();
        try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             Scanner scanner = new Scanner(reader)
        ) {
            String[] columnNames = DELIMITER.split(scanner.nextLine());
            while (scanner.hasNextLine()) {
                String row = scanner.nextLine();
                Optional<Patient> patient = patientFromRow(columnNames, row);
                try {
                    if (patient.isPresent()) {
                        register.addPatient(patient.get());
                    }
                } catch (AlreadyExistsException e) {
                    throw new IllegalFormatException("Duplicate patients were present.", e);
                }
            }
        }
        return register;
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
