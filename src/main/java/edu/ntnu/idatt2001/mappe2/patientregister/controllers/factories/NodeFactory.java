package edu.ntnu.idatt2001.mappe2.patientregister.controllers.factories;

import edu.ntnu.idatt2001.mappe2.patientregister.controllers.FontAwesomeIcon;
import edu.ntnu.idatt2001.mappe2.patientregister.models.Patient;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class NodeFactory {

    /*
    <VBox xmlns:fx="http://javafx.com/fxml"
        alignment="center"
        prefWidth="600" prefHeight="400">
    <fx:define>
        <String fx:id="faUserPlus" fx:value="&#xf234;"/>
        <String fx:id="faUserMinus" fx:value="&#xf503;"/>
        <String fx:id="faUserEdit" fx:value="&#xf4ff;"/>
    </fx:define>
    <stylesheets>
        <URL value="@../style.css"/>
    </stylesheets>
    <MenuBar>
        <Menu text="File">
            <MenuItem text="Import from .CSV" onAction="#onImportFromCSV">
            </MenuItem>
            <MenuItem text="Export to .CSV" onAction="#onExportToCSV">
            </MenuItem>
            <SeparatorMenuItem/>
            <MenuItem text="Exit" onAction="#onExit">
            </MenuItem>
        </Menu>
        <Menu text="Edit">
            <MenuItem text="Add new patient" onAction="#onAddPatient">
                <accelerator>
                    <KeyCodeCombination code="A" shift="UP" control="UP" alt="UP" meta="UP" shortcut="UP"/>
                </accelerator>
                <graphic>
                    <Text text="$faUserPlus" styleClass="fa-solid-icon"/>
                </graphic>
            </MenuItem>
            <MenuItem text="Edit selected patient" onAction="#onEditPatient">
                <accelerator>
                    <KeyCodeCombination code="E" shift="UP" control="UP" alt="UP" meta="UP" shortcut="UP"/>
                </accelerator>
                <graphic>
                    <Text text="$faUserEdit" styleClass="fa-solid-icon"/>
                </graphic>
            </MenuItem>
            <MenuItem text="Remove selected patient" onAction="#onRemovePatient">
                <accelerator>
                    <KeyCodeCombination code="DELETE" shift="UP" control="UP" alt="UP" meta="UP" shortcut="UP"/>
                </accelerator>
                <graphic>
                    <Text text="$faUserMinus" styleClass="fa-solid-icon"/>
                </graphic>
            </MenuItem>
        </Menu>
        <Menu text="Help">
            <MenuItem text="About" onAction="#onAbout"/>
        </Menu>
    </MenuBar>
    <HBox>
        <Button onAction="#onAddPatient" text="$faUserPlus" styleClass="fa-solid-icon"/>
        <Button onAction="#onRemovePatient" text="$faUserMinus" styleClass="fa-solid-icon"/>
        <Button onAction="#onEditPatient" text="$faUserEdit" styleClass="fa-solid-icon"/>
    </HBox>
    <TableView fx:id="patientTableView" VBox.vgrow="ALWAYS">
        <columns>
            <TableColumn text="First name" fx:id="firstNameColumn"/>
            <TableColumn text="Last name" fx:id="lastNameColumn"/>
            <TableColumn text="Social security number" fx:id="socialSecurityNumberColumn"/>
            <TableColumn text="Diagnosis" fx:id="diagnosisColumn"/>
            <TableColumn text="General pracitioner" fx:id="generalPractitionerColumn"/>
        </columns>
    </TableView>
    <BorderPane style="-fx-background-color: #444444">
        <left>
            <Label fx:id="statusLabel" textFill="white"/>
        </left>
    </BorderPane>
</VBox>

     */
    private Node createMenuBar() {
        MenuItem importMenuItem = new MenuItem("Import from .CSV");
        importMenuItem.setId("import-menu-item");
        MenuItem exportMenuItem = new MenuItem("Export to .CSV");
        exportMenuItem.setId("export-menu-item");
        MenuItem exitMenuItem = new MenuItem("Exit");
        exitMenuItem.setId("exit-menu-item");
        Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(
            importMenuItem,
            exportMenuItem,
            new SeparatorMenuItem(),
            exitMenuItem
        );

        MenuItem addPatientMenuItem = new MenuItem("Add new patient");
        addPatientMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.A));
        addPatientMenuItem.setGraphic(new Text(FontAwesomeIcon.USER_PLUS.toString()));
        addPatientMenuItem.setId("addPatientMenuItem");
        MenuItem editPatientMenuItem = new MenuItem("Edit selected patient");
        editPatientMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        editPatientMenuItem.setGraphic(new Text(FontAwesomeIcon.USER_EDIT.toString()));
        editPatientMenuItem.setId("editPatientMenuItem");
        MenuItem removePatientMenuItem = new MenuItem("Remove selected patient");
        removePatientMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        removePatientMenuItem.setGraphic(new Text(FontAwesomeIcon.USER_MINUS.toString()));
        removePatientMenuItem.setId("removePatientMenuItem");
        Menu editMenu = new Menu("Edit");
        editMenu.getItems().addAll(
            addPatientMenuItem,
            editPatientMenuItem,
            removePatientMenuItem
        );

        MenuItem aboutMenuItem = new MenuItem("About");
        aboutMenuItem.setId("aboutMenuItem");
        Menu aboutMenu = new Menu("About");
        aboutMenu.getItems().add(aboutMenuItem);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, editMenu, aboutMenu);
        return menuBar;
    }

    private Node createActionButtons() {
        Button addButton = new Button(FontAwesomeIcon.USER_PLUS.toString());
        addButton.setId("addPatientButton");
        Button removeButton = new Button(FontAwesomeIcon.USER_MINUS.toString());
        removeButton.setId("removePatientButton");
        Button editButton = new Button(FontAwesomeIcon.USER_EDIT.toString());
        editButton.setId("editPatientButton");
        addButton.getStyleClass().add("fa-solid-icon");
        removeButton.getStyleClass().add("fa-solid-icon");
        editButton.getStyleClass().add("fa-solid-icon");

        HBox hBox = new HBox();
        hBox.getChildren().addAll(addButton, removeButton, editButton);
        return hBox;
    }

    private Node createStatusBar() {
        Label statusLabel = new Label();
        statusLabel.setId("statusLabel");
        statusLabel.setTextFill(Color.WHITE);

        BorderPane borderPane = new BorderPane();
        borderPane.setStyle("-fx-background-color: #444444;");
        borderPane.setLeft(statusLabel);

        return borderPane;
    }

    private Node createPatientTableView() {
        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First name");
        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last name");
        TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("Social security number");
        TableColumn<Patient, String> diagnosisColumn = new TableColumn<>("Diagnosis");
        TableColumn<Patient, String> generalPractitionerColumn = new TableColumn<>("General practitioner");
        firstNameColumn.setId("firstNameColumn");
        lastNameColumn.setId("lastNameColumn");
        socialSecurityNumberColumn.setId("socialSecurityNumberColumn");
        generalPractitionerColumn.setId("generalPractitionerColumn");

        TableView<Patient> tableView = new TableView<>();
        tableView.setId("patientTableView");
        tableView.getColumns().addAll(
            firstNameColumn,
            lastNameColumn,
            socialSecurityNumberColumn,
            diagnosisColumn,
            generalPractitionerColumn
        );
        return tableView;
    }

    /**
     * Creates a new Node based on the NodeType.
     * @param nodeType The type of Node to create.
     * @return New Node of the specified type.
     * @throws IllegalArgumentException The NodeType is not valid.
     */
    public Node getNode(NodeType nodeType) {
        switch (nodeType) {
            case STATUS_BAR: return createStatusBar();
            case PATIENT_TABLE_VIEW: return createPatientTableView();
            case ACTION_BUTTONS: return createActionButtons();
            case MENU_BAR: return createMenuBar();
            default: throw new IllegalArgumentException(nodeType + " unknown node type");
        }
    }
}
