package edu.ntnu.idatt2001.mappe2.patientregister.models.serialization;

import edu.ntnu.idatt2001.mappe2.patientregister.models.Patient;
import edu.ntnu.idatt2001.mappe2.patientregister.models.PatientRegister;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.AlreadyExistsException;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterCSVSerializerTest {

    @Test
    public void serialize_writesPatientsToStreamAsCSV() throws IOException, AlreadyExistsException {
        PatientRegister register = new PatientRegister();
        register.addPatient(new Patient(
            "Joseph",
            "Joestar",
            "12345678912"
        ));
        register.addPatient(new Patient(
            "Jonathan",
            "Joestar",
            "12345678913",
            "Bronchitis",
            "Lisa Lisa"
        ));
        String expectedData = (
            "firstName;lastName;socialSecurityNumber;diagnosis;generalPractitioner\n" +
            "Joseph;Joestar;12345678912;;\n" +
            "Jonathan;Joestar;12345678913;Bronchitis;Lisa Lisa"
        );
        String actualData;

        try (
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PatientRegisterCSVSerializer serializer = new PatientRegisterCSVSerializer(outputStream)
        ) {
            serializer.serialize(register);
            actualData = new String(outputStream.toByteArray(), StandardCharsets.UTF_8);
        }

        assertEquals(expectedData, actualData);
    }
}