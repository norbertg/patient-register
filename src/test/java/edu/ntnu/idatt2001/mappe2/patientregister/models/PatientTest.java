package edu.ntnu.idatt2001.mappe2.patientregister.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {
    @Test
    public void constructor_nullFirstName_throwsException() {
        assertThrows(
            NullPointerException.class,
            () -> new Patient(
                null,
                "Johnson",
                "10431298051",
                "yes",
                "yes"
            )
        );
    }

    @Test
    public void constructor_nullLastName_throwsException() {
        assertThrows(
            NullPointerException.class,
            () -> new Patient(
                "Alison",
                null,
                "10431298051",
                "",
                ""
            )
        );
    }

    @Test
    public void constructor_nullSocialSecurityNumber_throwsException() {
        assertThrows(
            NullPointerException.class,
            () -> new Patient(
                "Alison",
                "Johnson",
                null,
                "",
                ""
            )
        );
    }

    @Test
    public void constructor_nullDiagnosis_throwsException() {
        assertThrows(
            NullPointerException.class,
            () -> new Patient(
                "Alison",
                "Johnson",
                "10431298051",
                null,
                ""
            )
        );
    }

    @Test
    public void constructor_nullGeneralPractitioner_throwsException() {
        assertThrows(
            NullPointerException.class,
            () -> new Patient(
                "Alison",
                "Johnson",
                "10431298051",
                "",
                null
            )
        );
    }

    @Test
    public void constructor_non11DigitSocialSecurityNumber_throwsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> new Patient("Alison", "Johnson", "1234563789")
        );
    }

    @Test
    public void constructor_blankSocialSecurityNumber_throwsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> new Patient("Alison", "Johnson", " ")
        );
    }

    @Test
    public void constructor_nonBlankNamesAndNonBlankSocialSecurityNumber_doesNotThrowException() {
        new Patient("Alison", "Garrison", "09876543210");
    }

    @Test
    public void getFirstName_returnsFirstName() {
        Patient patient = new Patient("Peter", "Johnson", "09876543210");

        String firstName = patient.getFirstName();

        assertEquals("Peter", firstName);
    }

    @Test
    public void setFirstName_nullName_throwsException() {
        Patient patient = new Patient("Alison", "Garrison", "10432980851");

        assertThrows(NullPointerException.class, () -> patient.setFirstName(null));
    }

    @Test
    public void setFirstName_blankName_throwsException() {
        Patient patient = new Patient("Alison", "Garrison", "10473298051");

        assertThrows(IllegalArgumentException.class, () -> patient.setFirstName(" "));
    }

    @Test
    public void setFirstName_nonBlankName_setsFirstName() {
        Patient patient = new Patient("Alison", "Garrison", "10436298051");

        patient.setFirstName("Peter");
        String firstName = patient.getFirstName();

        assertEquals("Peter", firstName);
    }

    @Test
    public void getLastName_returnsTheLastName() {
        Patient patient = new Patient("Alison", "Garrison", "10432931805");

        String lastName = patient.getLastName();

        assertEquals("Garrison", lastName);
    }

    @Test
    public void setLastName_nullName_throwsException() {
        Patient patient = new Patient("Alison", "Garrison", "10432951805");

        assertThrows(NullPointerException.class, () -> patient.setLastName(null));
    }

    @Test
    public void setLastName_blankName_throwsException() {
        Patient patient = new Patient("Alison", "Garrison", "10413298015");

        assertThrows(IllegalArgumentException.class, () -> patient.setLastName(" "));
    }

    @Test
    public void setLastName_nonBlankName_setsTheName() {
        Patient patient = new Patient("Alison", "Garrison", "10431298051");

        patient.setLastName("Johnson");
        String lastName = patient.getLastName();

        assertEquals("Johnson", lastName);
    }

    @Test
    public void getDiagnosis_returnsTheDiagnosis() {
        Patient patient = new Patient(
            "Alison",
            "Garrison",
            "10432931805",
            "Bronchitis",
            ""
        );

        String diagnosis = patient.getDiagnosis();

        assertEquals("Bronchitis", diagnosis);
    }

    @Test
    public void getGeneralPractitioner_returnsTheGeneralPracitioner() {
        Patient patient = new Patient(
            "Alison",
            "Garrison",
            "10432931805",
            "",
            "Joseph Joestar"
        );

        String generalPractitioner = patient.getGeneralPractitioner();

        assertEquals("Joseph Joestar", generalPractitioner);
    }

    @Test
    public void setDiagnosis_nullDiagnosis_throwsException() {
        Patient patient = new Patient("Alison", "Garrison", "10431298051");

        assertThrows(NullPointerException.class, () -> patient.setDiagnosis(null));
    }

    @Test
    public void setDiagnosis_nonNullDiagnosis_setsDiagnosis() {
        Patient patient = new Patient("Alison", "Garrison", "10431298051");
        String expectedDiagnosis = "Coronavirus";

        patient.setDiagnosis(expectedDiagnosis);
        String diagnosis = patient.getDiagnosis();

        assertEquals(expectedDiagnosis, diagnosis);
    }

    @Test
    public void setGeneralPractitioner_nullGeneralPractitioner_throwsException() {
        Patient patient = new Patient("Alison", "Garrison", "10431298051");

        assertThrows(NullPointerException.class, () -> patient.setGeneralPractitioner(null));
    }

    @Test
    public void setGeneralPractitioner_nullGeneralPractitioner_setsGeneralPracitioner() {
        Patient patient = new Patient("Alison", "Garrison", "10431298051");
        String expectedGeneralPractitioner = "Coronavirus";

        patient.setGeneralPractitioner(expectedGeneralPractitioner);
        String generalPractitioner = patient.getGeneralPractitioner();

        assertEquals(expectedGeneralPractitioner, generalPractitioner);
    }
}