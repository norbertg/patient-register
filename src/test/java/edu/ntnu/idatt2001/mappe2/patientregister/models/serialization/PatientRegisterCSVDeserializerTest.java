package edu.ntnu.idatt2001.mappe2.patientregister.models.serialization;

import edu.ntnu.idatt2001.mappe2.patientregister.models.Patient;
import edu.ntnu.idatt2001.mappe2.patientregister.models.PatientRegister;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.IllegalFormatException;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterCSVDeserializerTest {

    @Test
    public void deserialize_readsPatientRegisterFromStream() throws IOException {
        String csv = "firstName;lastName;generalPractitioner;socialSecurityNumber\n" +
            "Andrø;Tanker;Maren G. Skake;29104300764\n" +
            "Ane;Rikke;Dr. Klø;12073004096";
        byte[] csvBytes = csv.getBytes(StandardCharsets.UTF_8);
        Set<Patient> expectedPatients = Set.of(
            new Patient(
                "Andrø",
                "Tanker",
                "29104300764",
                "",
                "Maren G. Skake"
            ),
            new Patient(
                "Ane",
                "Rikke",
                "12073004096",
                "",
                "Dr. Klø"
            )
        );

        PatientRegister register;
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(csvBytes);
            PatientRegisterCSVDeserializer deserializer = new PatientRegisterCSVDeserializer(inputStream)) {
            register = deserializer.deserialize();
        }
        Set<Patient> patients = new HashSet<>(register.getPatients());

        assertEquals(expectedPatients, patients);
    }

    @Test
    public void deserialize_trailingNewLine_readsPatientRegisterFromStream() throws IOException {
        String csv = "firstName;lastName;generalPractitioner;socialSecurityNumber\n" +
            "Andrø;Tanker;Maren G. Skake;29104300764\n" +
            "Ane;Rikke;Dr. Klø;12073004096\n\n\n\n";
        byte[] csvBytes = csv.getBytes(StandardCharsets.UTF_8);
        Set<Patient> expectedPatients = Set.of(
            new Patient(
                "Andrø",
                "Tanker",
                "29104300764",
                "",
                "Maren G. Skake"
            ),
            new Patient(
                "Ane",
                "Rikke",
                "12073004096",
                "",
                "Dr. Klø"
            )
        );

        PatientRegister register;
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(csvBytes);
             PatientRegisterCSVDeserializer deserializer = new PatientRegisterCSVDeserializer(inputStream)) {
            register = deserializer.deserialize();
        }
        Set<Patient> patients = new HashSet<>(register.getPatients());

        assertEquals(expectedPatients, patients);
    }

    @Test
    public void deserialize_invalidSocialSecurityNumber_throwsIllegalFormatException() throws IOException {
        String csv = "firstName;lastName;generalPractitioner;socialSecurityNumber\n" +
            "Joseph;Joestar;Jonathan Joestar;3450\n";
        byte[] csvBytes = csv.getBytes(StandardCharsets.UTF_8);

        PatientRegister register;
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(csvBytes);
             PatientRegisterCSVDeserializer deserializer = new PatientRegisterCSVDeserializer(inputStream)) {
            assertThrows(IllegalFormatException.class, () -> deserializer.deserialize());
        }
    }

    @Test
    public void deserialize_invalidRow_throwsIllegalFormatException() throws IOException {
        String csv = "firstName;lastName;generalPractitioner;socialSecurityNumber\n" +
            "Joseph;Joestar\n";
        byte[] csvBytes = csv.getBytes(StandardCharsets.UTF_8);

        PatientRegister register;
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(csvBytes);
             PatientRegisterCSVDeserializer deserializer = new PatientRegisterCSVDeserializer(inputStream)) {
            assertThrows(IllegalFormatException.class, () -> deserializer.deserialize());
        }
    }
}