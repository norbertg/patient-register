package edu.ntnu.idatt2001.mappe2.patientregister.models;

import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.AlreadyExistsException;
import edu.ntnu.idatt2001.mappe2.patientregister.models.exceptions.NotExistsException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {
    @Test
    public void addPatient_nullPatient_throwsException() {
        PatientRegister register = new PatientRegister();

        assertThrows(NullPointerException.class, () -> register.addPatient(null));
    }

    @Test
    public void addPatient_nonExistingPatient_doesNotThrow() {
        PatientRegister register = new PatientRegister();
        Patient patient = new Patient("Joseph", "Joestar", "12345678912");

        assertDoesNotThrow(() -> register.addPatient(patient));
    }

    @Test
    public void addPatient_existingPatient_throwsException() throws AlreadyExistsException {
        PatientRegister register = new PatientRegister();
        Patient patient = new Patient("Joseph", "Joestar", "12345678912");

        register.addPatient(patient);
        assertThrows(AlreadyExistsException.class, () -> register.addPatient(patient));
    }

    @Test
    public void getPatients_returnsPatients() throws AlreadyExistsException {
        PatientRegister register = new PatientRegister();
        Patient patient = new Patient("Joseph", "Joestar", "12345678912");
        List<Patient> expectedPatients = List.of(patient);

        register.addPatient(patient);
        List<Patient> patients = register.getPatients();

        assertEquals(expectedPatients, patients);
    }

    @Test
    public void removePatient_nullPatient_throwsException() {
        PatientRegister register = new PatientRegister();

        assertThrows(NullPointerException.class, () -> register.removePatient(null));
    }

    @Test
    public void removePatient_existingPatient_removesPatient() throws AlreadyExistsException, NotExistsException {
        PatientRegister register = new PatientRegister();
        Patient patient = new Patient("Joseph", "Joestar", "12345678912");

        register.addPatient(patient);
        register.removePatient(patient);
        List<Patient> patients = register.getPatients();

        assertEquals(List.of(), patients);
    }

    @Test
    public void removePatient_nonExistingPatient_throwsException() {
        PatientRegister register = new PatientRegister();
        Patient patient = new Patient("Joseph", "Joestar", "12345678912");

        assertThrows(NotExistsException.class, () -> register.removePatient(patient));
    }
}